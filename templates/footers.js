const footerUniBerlin = `
    <footer class="text-center text-lg-start bg-light berlin-footer">
    <!-- Section: Social media -->
    <section class="d-flex justify-content-center justify-content-lg-between p-4">

    </section>
    <!-- Section: Social media -->
    
    <!-- Section: Links  -->
    <section class="">
        <div class="container text-center text-md-start mt-5">
            <!-- Grid row -->
            <div class="row mt-3">

                <!-- Grid column -->
                <div class="col-md-2 col-lg-4 col-xl-4 mx-auto mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">
                        Our university
                    </h6>
                    <p>
                        <span>Why us?</span>
                    </p>
                    <p>
                        <span>Our team</span>
                    </p>
                    <p>
                        <span>Services and facilities</span>
                    </p>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-3 col-lg-4 col-xl-4 mx-auto mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">
                        Studies
                    </h6>
                    <p>
                        <span>Bachelor</span>
                    </p>
                    <p>
                        <span>Master</span>
                    </p>
                    <p>
                        <span>Short courses</span>
                    </p>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">Contact</h6>
                    <p>
                        hello@berlinuniversity.com
                    </p>
                    <p>+49 234 567 89</p>
                </div>
                <!-- Grid column -->
            </div>
            <!-- Grid row -->
        </div>
    </section>
    <!-- Section: Links  -->

    <!-- Copyright -->
    <div class="text-white py-4 col-10 m-auto text-center bg-uni-de">
        <div class="container">
            <div class="row mt-3">

                <!-- Grid column -->
                <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">
                        <img src="/study-financing/imagesfinancing/images/flags/de.svg" alt="30" height="40"> German Financial Aid & Scholarships
                    </h6>
                    <p>
                        <span>© 2023 </span>
                    </p>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">
                        &nbsp;
                    </h6>
                    <p class="text-decoration-underline">
                        <span>Cookies</span>
                    </p>
                    <p>
                </div>
                <!-- Grid column -->
                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">&nbsp;</h6>
                    <p class="text-decoration-underline">Data Privacy Policy</p>
                </div>
                <!-- Grid column -->
                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">&nbsp;</h6>
                    <p class="text-decoration-underline"></i>Legal Notice</p>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">&nbsp;</h6>
                    <p class="text-decoration-underline">Contact</p>
                </div>
                <!-- Grid column -->
                <!-- Grid column -->
                <div class="col-md-1 col-lg-1 col-xl-1 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">&nbsp;</h6>
                    <p class="text-decoration-underline">Career</p>
                </div>
                <!-- Grid column -->


            </div>
            <!-- Grid row -->
        </div>
    </div>
    <!-- Copyright -->
</footer>
`

const footerUniBrussels = `
    footer brussels
`

const footerEidasDE = `
    <footer class="text-center text-lg-start berlin-footer bg-head-foot-eidas-de">
        <section>
        <div class="text-white py-4 col-10 m-auto text-center">
            <div class="row mt-3">

                <!-- Grid column -->
                <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">
                        © Federal Ministry of the Interior and Community, 2023
                    </h6>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">
                        <span>Site notice</span>
                    </p>
                    <p>
                </div>
                <!-- Grid column -->
                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">Privacy</p>
                </div>
                <!-- Grid column -->
                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline"></i>Accessibility statement</p>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">Contact</p>
                </div>


            </div>
            <!-- Grid row -->
    </div>
        </section>
    </footer>
`

const footerEidasIT = `
    <footer class="text-center text-lg-start berlin-footer bg-head-foot-eidas-it">
        <section>
        <div class="text-white py-4 col-10 m-auto text-center">
            <div class="row mt-3">

                <!-- Grid column -->
                <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">
                        <img src="/study-financing/imagesfinancing/images/governo-italiano.png" height="84" alt="">
                    </h6>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">
                        <span>Site notice</span>
                    </p>
                    <p>
                </div>
                <!-- Grid column -->
                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">Privacy</p>
                </div>
                <!-- Grid column -->
                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline"></i>Accessibility statement</p>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">Contact</p>
                </div>


            </div>
            <!-- Grid row -->
    </div>
        </section>
    </footer>
`


const footerEvReqDE = `
    <footer class="text-center text-lg-start berlin-footer bg-head-foot-eidas-de bg-foot-ev-req-de">
        <section>
        <div class="text-white py-4 col-10 m-auto text-center">
            <div class="row mt-3">

                <!-- Grid column -->
                <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">
                        <img src="/study-financing/imagesfinancing/images/flags/de.svg" alt=""/>Evidence Requester Service
                    </h6>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">
                        <span>Site notice</span>
                    </p>
                    <p>
                </div>
                <!-- Grid column -->
                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">Privacy</p>
                </div>
                <!-- Grid column -->
                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline"></i>Accessibility statement</p>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">Contact</p>
                </div>
            </div>
            <!-- Grid row -->
    </div>
        </section>
</footer>`;


const footerEvReqIT = `
    <footer class="text-center text-lg-start berlin-footer bg-head-foot-eidas-de bg-head-foot-eidas-it">
        <section>
        <div class="text-white py-4 col-10 m-auto text-center">
            <div class="row mt-3">

                <!-- Grid column -->
                <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4">
                    <!-- Links -->
                    <h6 class="bold mb-4">
                        <img src="/study-financing/imagesfinancing/images/flags/it.svg" alt=""/>National preview space of Italy
                    </h6>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">
                        <span>Site notice</span>
                    </p>
                    <p>
                </div>
                <!-- Grid column -->
                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">Privacy</p>
                </div>
                <!-- Grid column -->
                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline"></i>Accessibility statement</p>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <p class="text-decoration-underline">Contact</p>
                </div>
            </div>
            <!-- Grid row -->
    </div>
        </section>
</footer>`;


export const footers = {
    'footerUniBerlin': footerUniBerlin,
    'footerUniBrussels': footerUniBrussels,
    'footerEidasDE': footerEidasDE,
    'footerEidasIT': footerEidasIT,
    'footerEvidenceRequestDE': footerEvReqDE,
    'footerEvidenceRequestIT': footerEvReqIT,
}
