const headerUniBerlin = `
    <nav class="navbar navbar-expand-sm navbar-light py-4 border-bottom uni-de"><!-- bg-light -->
        <div class="container-fluid">
            <div class="container">
                <div class="d-flex w-100 justify-content-between align-items-center" href="#">
                    <div class="d-flex align-items-center">
                        <h5 class="m-0 ms-3 medium"><img src="/study-financing/imagesfinancing/images/flags/de.svg" alt="30" class="me-2"> German Financial Aid & Scholarships </h5>
                    </div>
                    <div class="d-flex justify-content-evenly gap-5">
                        <div>Bachelor</div>
                        <div>Master</div>
                        <div>MBA + DBA</div>
                        <div>Dual Studies</div>
                        <div class="language fs-6">
                            DE | EN 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
`

const headerUniBrussels = `
    <nav class="navbar navbar-expand-sm navbar-light py-4 border-bottom uni-be"><!-- bg-light -->
    <div class="container-fluid">
        <div class="container">
            <div class="d-flex w-100 justify-content-between align-items-center" href="#">
                <div class="d-flex align-items-center justify-content-around">
                    <h5 class="m-0 ms-3 bold">Brussels University</h5>
                </div>
                <div class="d-flex justify-content-evenly gap-5">
                    <div>Study</div>
                    <div>Research</div>
                    <div>Innovation</div>
                    <div>International relations</div>
                    <div class="language fs-6">
                        | EN 
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
`

const headerEidasDE = `
    <nav class="navbar navbar-expand-sm navbar-light py-4 border-bottom bg-head-foot-eidas-de"><!-- bg-light -->
    <div class="container-fluid">
        <div class="container">
            <div class="d-flex w-100 justify-content-between align-items-center" href="#">
                <div class="d-flex align-items-center justify-content-around">
                <img src="/study-financing/imagesfinancing/images/bundID-white-logo.svg" height="64" />
                        <h5 class="m-0 ms-3 bold"></h5>
                </div>
                <div class="d-flex justify-content-evenly gap-5 text-white">
                    <div class="language fs-6">
                        ENGLISH 
                    </div>
                    <div class="language fs-6 text-decoration-underline">
                        HELP 
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</nav>
`

const headerEidasIT = `
    <nav class="navbar navbar-expand-sm navbar-light py-4 border-bottom bg-head-foot-eidas-it"><!-- bg-light -->
    <div class="container-fluid">
        <div class="container">
            <div class="d-flex w-100 justify-content-between align-items-center" href="#">
                <div class="d-flex align-items-center justify-content-around">
                <img src="/study-financing/imagesfinancing/images/governo-italiano.png" height="84">
                        <h5 class="m-0 ms-3 bold"></h5>
                </div>
                <div class="d-flex justify-content-evenly gap-5 text-white">
                    <div class="language fs-6">
                        ENGLISH 
                    </div>
                    <div class="language fs-6 text-decoration-underline">
                        HELP 
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</nav>
`



const headerEvidenceRequestDE = `
    <nav class="navbar navbar-expand-sm navbar-light py-4 border-bottom bg-white"><!-- bg-light -->
    <div class="container-fluid">
        <div class="container">
            <div class="d-flex w-100 justify-content-between align-items-center" href="#">
                <div class="d-flex align-items-center justify-content-around">
                <img src="/study-financing/imagesfinancing/images/federal-de.svg" height="92">
                        <h5 class="m-0 ms-3"><img src="/study-financing/imagesfinancing/images/flags/de.svg" alt="30" class="me-2">Evidence request service </h5>
                </div>
                <div class="d-flex justify-content-evenly gap-1">
                    <div class="language fs-6">
                        EN  | 
                    </div>
                    <div class="language fs-6">
                         Choose language
                         
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</nav>
`

const headerEvidenceRequestIT = `
    <nav class="navbar navbar-expand-sm navbar-light py-4 border-bottom bg-white"><!-- bg-light -->
    <div class="container-fluid">
        <div class="container">
            <div class="d-flex w-100 justify-content-between align-items-center" href="#">
                <div class="d-flex align-items-center justify-content-around">
                <img src="/study-financing/imagesfinancing/images/governo-italiano.png" height="84">
                        <h5 class="m-0 ms-3"><img src="/study-financing/imagesfinancing/images/flags/it.svg" alt="30" class="me-2">National preview space of Italy </h5>
                </div>
                <div class="d-flex justify-content-evenly gap-1">
                    <div class="language fs-6">
                        EN  | 
                    </div>
                    <div class="language fs-6">
                         Choose language
                         
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</nav>
`




export const headers  = {
    'headerUniBerlin' : headerUniBerlin,
    'headerUniBrussels' : headerUniBrussels,
    'headerEidasDE' : headerEidasDE,
    'headerEidasIT' : headerEidasIT,
    'headerEvidenceRequestDE' : headerEvidenceRequestDE,
    'headerEvidenceRequestIT' : headerEvidenceRequestIT
}
