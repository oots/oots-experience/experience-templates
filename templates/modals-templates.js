import {countries} from "../countries.js";

const modalConsentToRequestHTML = `
    <div class="modal" tabindex="-1" role="dialog" id="consent-to-request">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body p-5">
                        <h1 class="text-center">
                            Consent to request data
                        </h1>
                        <h4 class="text-center my-4">
                            Can we request your personal data from ${countries.get(sessionStorage.getItem('countrySendingData'))} 
                            and share it with ${countries.get(sessionStorage.getItem('countryRequestingData'))}?
                        </h4>

                        <div class="m-auto w-75 p-3 bg-neutral-50 rounded-5 my-5">
                            <h6>
                                Your personal data to be requested:
                            </h6>
                            <ul class="fst-italic">
                                <li>current family name(s)</li>
                                <li>current first name(s)</li>
                                <li>date of birth</li>
                                <li>unique identifier</li>
                            </ul>
                        </div>
                        <div class="d-flex justify-content-center gap-3 mb-5">
                            <button type="button" class="btn btn-outline-primary">Cancel</button>
                            <button type="button" class="btn btn-primary open-modal" data-modalid="redirecting">Consent
                            </button>
                        </div>
                        <div class="mt-3">
                            <p>Learn more about how we collect, use, share and protect your personal data on our Privacy
                                Policy.</p>
                            <p>Learn more about cross-border authentication</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
`

const modalConsentToShareHTML = `
    <div class="modal" tabindex="-1" role="dialog" id="consent-to-share">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body p-5">
                    <h1 class="text-center">
                        Your data is ready to share
                    </h1>
                    <h4 class="text-center my-4">
                        Do you consent to share your data in response to the request made by a public service in
                        ${countries.get(sessionStorage.getItem('countryRequestingData'))}?
                    </h4>

                    <div class="m-auto w-75 p-5 bg-neutral-50 rounded-5 my-5">
                        <h6 class="bold">
                            Your personal data to be requested:
                        </h6>
                        <p class="mb-0 fst-italic">First name and Last Name: Anika De Boer</p>
                        <p class="mb-0 fst-italic">Nationality: Italian</p>
                        <p class="mb-0 fst-italic">Date of birth: 08/12/1975</p>
                        <p class="mb-0 fst-italic">Unique Identifier: 98273489327498</p>
                        <div class="p-3 my-5">
                            The information will be shared with a high level of assurance.
                        </div>

                        <div class="d-flex justify-content-center gap-3 mb-5">
                            <button type="button" class="btn btn-outline-primary">Cancel</button>
                            <button type="button" class="btn btn-primary open-modal" data-modalid="redirecting">Consent
                            </button>
                        </div>

                    </div>
                    <div class="m-auto w-75">
                        <p>Learn more about how we collect, use, share and protect your personal data on our Privacy
                            Policy.</p>
                        <p>Learn more about cross-border authentication</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

`

const modalRedirect = `
    <div class="modal" tabindex="-1" role="dialog" id="redirecting">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body p-5">
                        <h1 class="text-center">
                            We are redirecting you
                        </h1>
                        <div class="d-flex justify-content-center my-3 gap-4">
                            <div class="d-flex flex-column text-center col-4">
                                <div class="regular">FROM</div>
                                <div class="bold my-3">National Authentication Service</div>
                                <img src="images/flags/de.svg" height="30">
                            </div>
                            <div class="arrows-animation col-3">
                                <img class="first" src="images/arrow-right.svg" height="12">
                                <img class="second" src="images/arrow-right.svg" height="12">
                                <img class="third" src="images/arrow-right.svg" height="12">
                                <img class="forth" src="images/arrow-right.svg" height="12">
                            </div>
                            <div class="d-flex flex-column text-center col-4">
                                <div class="regular">TO</div>
                                <div class="bold my-3">National Authentication Service</div>
                                <img src="images/flags/be.svg" height="30">
                            </div>
                        </div>
                    </div>
                    <!--<div class="text-center">
                        <small class="m-0">Powered by</small>
                        <p><img src="/images/YourEurope.png" height="90" alt="Your Europe"></p>
                    </div>-->
                    <div class="modal-footer justify-content-center p-3 text-center bg-neutral-400 text-white">
                        <div><p class="fw-light">As part of the</p>
                            <p class="bold">Apply for study financing in Germany</p></div>
                    </div>
                </div>
            </div>
        </div>
`


const modalSharing = `
    <div class="modal" tabindex="-1" role="dialog" id="sharing">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body p-5 text-center">
                    <div class="d">
                        <img src="/study-financing/imagesfinancing/images/shared-check.svg" height="90">
                    </div>
                    <h3 class=" bold">
                        The document has been shared with 
                    </h3>
                    <p>Berlin University</p>
                    <div class="d-flex justify-content-center my-3 gap-4">
                        <div>
                            <p>You will be redirected to the Berlin University page to proceed with your application</p>
                        
                        </div>
                    </div>
            </div>
            
                <div class="modal-footer justify-content-center p-3 text-center">
                    <div class="text-center">
                        <small class="m-0">Powered by</small>
                        <p><img src="/study-financing/imagesfinancing/images/YourEurope.png" height="90" alt="Your Europe"></p>
                    </div>
                </div>
        </div>
    </div>
`


export let modalRedirectMethod = function (requester, sender) {
    const step = sessionStorage.getItem('step');
    const portal = sessionStorage.getItem('portal');
    const currentSite = sessionStorage.getItem('currentSite');
    const destination = sessionStorage.getItem('destination');
    const redirectToPreview = sessionStorage.getItem('redirectToPreview');
    let from, to = "";

    if (step === 'start') {
        from = to = "National authentication service";
    }

    if (step === 'personal-info') {
        from = "National authentication service";
        to = sessionStorage.getItem('procedurePortal');
    }

    if (step === 'retrieve') {
        from = "Document request service";
        to = "National authentication service";
    }

    if (step === 'request') {
        from = "Document request service";
        to = "National authentication service";
    }

    if (step === 'retrieve' && currentSite === 'eidas') {
        from = "National authentication service";
        to = "National authentication service";
    }

    if (step === 'retrieve' && portal === 'NAS') {
        from = "Evidence request service";
        to = "National authentication service";
    }

    if (step === 'retrieve' && portal === 'NAS' && currentSite === 'eidas') {
        from = "National authentication service";
        to = "Document preview space";
    }

    if (currentSite === 'eidas' && destination && destination.includes("NAS")) {
        from = "National authentication service";
        to = "National authentication service";
    }

    if (currentSite.includes("evidenceRequest")) {
        from = "Evidence request service";
        to = "National authentication service";
    }

    if (redirectToPreview && portal !== 'NAS') {
        from = "National authentication service";
        to = "Document preview space";
    }

    if (destination === 'evidence-request-service-de.html?step=share') {
        from = "Document preview space";
        to = "Document request space";
    }

    if (step === 'confirm') {
        from = "Document preview space";
        to = "Document request service";
    }

    if (requester === sender) {
        from = "Berlin University";
        to = "Document request service";
    }

    let yourEuropeLogoVisibility = from === 'National authentication service' || to === 'National authentication service' ? 'none' : 'block';

    return `
    <div class="modal" tabindex="-1" role="dialog" id="redirecting">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body p-5">
                    <h1 class="text-center">
                        We are redirecting you
                    </h1>
                    <div class="d-flex justify-content-around align-items-center my-3 gap-4 text-center">
                    <div class="p-3">
                        <div class="regular">FROM</div>
                        <div class="bold bg my-2">${from}</div>
                        ${countries.get(requester)} <img src="images/flags/${requester}.svg" height="30">
                    </div>
                    <div class="arrows-animation">
                        <img class="first" src="images/arrow-right.svg" height="12">
                        <img class="second" src="images/arrow-right.svg" height="12">
                        <img class="third" src="images/arrow-right.svg" height="12">
                        <img class="forth" src="images/arrow-right.svg" height="12">
                    </div>
                    
                    <div class="bg-neutral-100 rounded-5 p-3"">
                        <div class="regular">TO</div>
                        <div class="bold my-2">${to}</div>
                        ${countries.get(sender)} <img src="images/flags/${sender}.svg" height="30">
                    </div>
                    </div>
                    
                </div>
                <div class="text-center" style="display: ${yourEuropeLogoVisibility}">
                        <small class="m-0">Powered by</small>
                        <p><img src="/study-financing/imagesfinancing/images/YourEurope.png" height="90" alt="Your Europe"></p>
                </div>
                <div class="modal-footer justify-content-center p-3 text-center bg-neutral-400 text-white">
                    <div><p class="fw-light">As part of the</p>
                    <p class="bold">Apply for study financing in Germany</p></div>
                </div>
            </div>
        </div>
    </div>
`
}

export const modalsTemplates = {
    'modalConsentToRequest': modalConsentToRequestHTML,
    'modalConsentToShare': modalConsentToShareHTML,
    'modalSharingDocHTML': modalSharing,
    'modalRedirect': 'modalRedirect'
}

