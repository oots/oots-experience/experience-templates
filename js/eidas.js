const selectable = document.getElementById("active");
const continueBtn = document.getElementById("continue-bth");
const btnSelect = $(".btn-select");
let availableCountries = [
    'Belgium', 'Croatia', 'Czech Republic', 'Denmark', 'Estonia', 'Germany', 'Italy', 'Latvia',
    'Lithuania', 'Luxembourg', 'Malta', 'Netherlands', 'Portugal', 'Slovakia', 'Spain', 'Spain', 'Sweden',
];

let langArray = [];
$('.vodiapicker option').each(function () {
    let img = $(this).attr("data-thumbnail");
    let text = this.innerText;
    let value = $(this).val();
    let available = availableCountries.includes(value);
    let availableText = available ? '' : 'Not available yet';
    let disabled = !$(this).is(":disabled"); // this works as the other way around, disable means enable :)
    if (img) {
        img = '<img src="' + img + '" alt=""/>';
    } else {
        img = '';
    }
    let item = '<li value="' + value + '" data-disabled="'+ disabled +'"><p>' + img + '<span>' + text + '</span></p><p><small>'+ availableText +'</small></p></li>';
    langArray.push(item);
})

$('#a').html(langArray);

//Set the button value to the first el of the array
btnSelect.html(langArray[0]).val('Country');/*.attr('value', 'Select a country');*/


btnSelect.on('keyup', function (e) {
    console.log($(this).val());
    $('.lang-select').css('background-image', 'none');
    $(".b").show();
    const filter = $(this).val().toUpperCase();
    const div = document.getElementById("a");
    const lis = div.getElementsByTagName("li");
    for (let i = 0; i < lis.length; i++) {
        txtValue = lis[i].textContent || lis[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            lis[i].style.display = "";
        } else {
            lis[i].style.display = "none";
        }
    }
})

btnSelect.click(function () {
    if($(this).val() === 'Country') {
        $(this).val('');
        $('.lang-select').css('background-image', 'none');
    }
    $(".b").toggle();
});

$('#a li').click(function () {
    let img = $(this).find('img').attr("src");
    let value = $(this).attr('value');
    let disabled = $(this).attr('data-disabled');
    let text = this.innerText;
    let available = $(this).attr('data-available');
    let item = '<li><p><img src="' + img + '" alt="" disabled="'+ disabled +'"/><span></p>' + text + '</span>,<p><small>'+ available +'</small></p></li>';
    console.log(disabled);
    if(disabled === 'false') {
    //     $('.btn-select').val(value).attr('value', value);
        btnSelect.val(value);
        $('.lang-select').css('background-image', 'url(' + img +  ')');
        btnSelect.css('padding-left', '3rem');
        $(".b").toggle();
    }
    console.log(value);
    if (value === 'Belgium') {
        continueBtn.disabled = false;
    }
});

