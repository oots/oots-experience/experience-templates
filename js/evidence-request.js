const loading = document.getElementById('loading');

const url = new URL(window.location.href);
const page = url.pathname;
const btnSelect = $(".btn-select");
let availableCountries = [
    'Belgium', 'Croatia', 'Czech Republic', 'Denmark', 'Estonia', 'Germany', 'Italy', 'Latvia',
    'Lithuania', 'Luxembourg', 'Malta', 'Netherlands', 'Portugal', 'Slovakia', 'Spain', 'Spain', 'Sweden',
];

let langArray = [];
$('.vodiapicker option').each(function () {
    const issuerCountry = sessionStorage.getItem('issuer-country');
    let img = $(this).attr("data-thumbnail");
    let text = this.innerText;
    let value = $(this).val();
    let available = availableCountries.includes(value) || (value === 'France' && issuerCountry === 'fr')|| (value === 'Spain' && issuerCountry === 'es');
    let availableText = available ? '' : 'Not available yet';
    let disabled = !$(this).is(":disabled"); // this works as the other way around, disable means enable :)
    if (img) {
        img = '<img src="' + img + '" alt=""/>';
    } else {
        img = '';
    }
    let item = '<li value="' + value + '" data-disabled="' + disabled + '"><p>' + img + '<span>' + text + '</span></p><p><small>' + availableText + '</small></p></li>';
    langArray.push(item);
})

$('#a').html(langArray);

//Set the button value to the first el of the array
btnSelect.html(langArray[0]).val('Country');/*.attr('value', 'Select a country');*/


btnSelect.on('keyup', function (e) {
    console.log($(this).val());
    $('.lang-select').css('background-image', 'none');
    $(".b").show();
    const filter = $(this).val().toUpperCase();
    const div = document.getElementById("a");
    const lis = div.getElementsByTagName("li");
    for (let i = 0; i < lis.length; i++) {
        txtValue = lis[i].textContent || lis[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            lis[i].style.display = "";
        } else {
            lis[i].style.display = "none";
        }
    }
})

btnSelect.click(function () {
    if ($(this).val() === 'Country') {
        $(this).val('');
        $('.lang-select').css('background-image', 'none');
    }
    $(".b").toggle();
});

//change button stuff on click
$('#a li').click(function () {
    let img = $(this).find('img').attr("src");
    let value = $(this).attr('value');
    let disabled = $(this).attr('data-disabled');
    let text = this.innerText;
    let available = $(this).attr('data-available');
    let item = '<li><p><img src="' + img + '" alt="" disabled="' + disabled + '"/><span></p>' + text + '</span>,<p><small>' + available + '</small></p></li>';
    console.log(disabled);
    if (disabled === 'false') {
        //     $('.btn-select').val(value).attr('value', value);
        btnSelect.val(value);
        $('.lang-select').css('background-image', 'url(' + img + ')');
        btnSelect.css('padding-left', '3rem');
        $(".b").toggle();
    }
    let button = document.getElementById('search-button');

    if (value) {
        const regionWrapper = document.getElementById('region-wrapper');
        if (value === 'be' && sessionStorage.getItem('region')) {
            button.disabled = true;
            loading.style.display = 'block';
            setTimeout(function () {
                loading.style.display = 'none';
                regionWrapper.classList.remove('d-none');
                regionWrapper.classList.add('d-block');
                const regionSelect = document.getElementById('region');
                regionSelect.addEventListener('change', function () {
                    if (value) {
                        const alertRegion = document.getElementById('alertRegion');
                        alertRegion.style.display = 'none';
                        enableSearchButton(button);
                    }
                });
            }, 1250);
        } else if (value === 'Belgium' || value === 'France' || value === 'Spain') {
            if(regionWrapper) {
                regionWrapper.classList.add('d-none');
            }

            button.disabled = false;
            button.addEventListener('click', function (ev) {
                step1Method();
            });
        }

        if (value === sessionStorage.getItem('issuer-country')) {
            if (regionWrapper) {
                regionWrapper.classList.add('d-none');
            }
            button.disabled = false;
            button.addEventListener('click', function (ev) {
                step1Method();
            });
        }
    } else {
        button.disabled = true;
    }
});

function enableSearchButton(button) {
    button.disabled = false;
    button.addEventListener('click', function (ev) {
        step1Method();
        sessionStorage.clear();
    });
}


const subLists = document.querySelectorAll('.sub-list li');

for (let index = 0; index < subLists.length; index++) {
    subLists[index].classList.add('d-none');
}

function step1Method() {
    loading.style.display = "block";
    setTimeout(function () {
        if (sessionStorage.getItem('issuer-country') && sessionStorage.getItem('issuer-country')) { /* === 'es' */
            window.location.href = 'evidence-request-service-be-request.html';
        } else {
            window.location.href = 'evidence-request-service-de-request.html';
        }
    }, 5000);
}

/*if (page.includes('evidence-request-service-de-request.html')) { WE REMOVED THE STEPS ON THE LEFT, THAT'S WHY THIS IS NOT NEEDED ANYMORE
    let step1 = document.getElementById("step-1");
    let step2 = document.getElementById("step-2");
    step1.classList.remove('current');
    step2.classList.add('current');
    // subLists[0].classList.remove('d-none');
}
if (page.includes('evidence-preview')) {
    let step1 = document.getElementById("step-1");
    let step3 = document.getElementById("step-3");
    step1.classList.remove('current');
    step3.classList.add('current');
    /!*subLists[0].classList.remove('d-none');
    subLists[1].classList.remove('d-none');
    subLists[2].classList.remove('d-none');
    subLists[3].classList.remove('d-none');*!/
}*/

const requestButton = document.getElementById('request-button');
if (requestButton) {
    requestButton.addEventListener('click', function () {
        /*if (page.includes('second-document')) {
            openModal("redirecting", "evidence-preview-authenticate-be.html")
        } else {
            openModal("redirecting", "evidence-preview-authenticate-it.html");
        }*/
        if (sessionStorage.getItem('issuer-country') && sessionStorage.getItem('issuer-country')) {
            openModal("redirecting", "evidence-preview-authenticate-" + sessionStorage.getItem('issuer-country') + ".html");
        } else {
            openModal("redirecting", "evidence-preview-authenticate-be.html");
        }
    })
}

const signIn = document.getElementById('sign-in');
if (signIn) {
    signIn.addEventListener('click', function () {
        // openModal("redirecting", "identity-provider.html"); // Removed we go directly, don't show popups if not cross borders
        window.location.href = 'identity-provider.html';
    })
}


const checkboxes = $('input[name^=confirmDocumentCheckbox]');
if (checkboxes) {
    checkboxes.on('change', function () {
        const checkedOne = Array.prototype.slice.call(checkboxes).some(x => x.checked);
        confirmButton.disabled = !checkedOne;
    });
}

const confirmButton = document.getElementById('confirm-button');
/*if (confirmButton) {
    confirmButton.addEventListener('click', function () {
        if (sessionStorage.getItem('issuer-country') && sessionStorage.getItem('issuer-country') === 'es') {
            openModal("redirecting", "second-document/procedure-portal-provide-document-second.html");
        } else {
            openModal("redirecting", "second-document/procedure-portal-provide-document-second.html");
        }
    })
}*/

function sendDoc() {
    closeAllModals();
    let loading = document.getElementById("loading");
    let modalDiv = document.getElementById('sending');
    loading.style.display = 'block';
    modalDiv.style.display = 'block';
    setTimeout(function () {
        window.location.href = window.location.pathname.indexOf('second-document') > -1 ? 'procedure-portal-ready-to-submit.html' : 'second-document/procedure-portal-provide-document-second.html';
    }, timeout)
}
