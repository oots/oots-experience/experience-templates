const url = new URL(window.location.href);
const searchParams = url.searchParams;

if (searchParams.get('popup')) {
    if (!sessionStorage.getItem('issuer-country')) {
        if (sessionStorage.getItem('destination-after-sign-in')) {
            openModal('consent-to-share', sessionStorage.getItem('destination-after-sign-in'));
        } else {
            openModal('consent-to-share');
        }
    } else {
        closeAllModals();
        let loading = document.getElementById("loading");
        let modalDiv = document.getElementById('consent-to-share');
        loading.style.display = modalDiv.style.display = 'block';
        document.getElementById('consent-to-share-btn').addEventListener('click', function () {
            window.location.href = 'evidence-preview-confirm-'+ sessionStorage.getItem('issuer-country') +'.html';
        });
    }

}
