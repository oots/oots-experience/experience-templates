// If absolute URL from the remote server is provided, configure the CORS
// header on that server.

// Loaded via <script> tag, create shortcut to access PDF.js exports.
// let pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
// pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

$(function () {

    $('body').append('<div id="viewer" class="viewer">\n' +
        '    <header class="viewer__toolbar">\n' +/*
        '        <button class="btn btn-secondary">Previous</button>\n' +
        '        <button class="btn btn-secondary">Next</button>\n' +
        '        <button class="btn btn-secondary">Print</button>\n' +*/
        '        <button class="btn btn-secondary close-preview"><span class="ms-2 medium">Close</span></button>\n' +
        '    </header>\n' +
        '    <main class="viewer__content close-preview">\n' +
        '        <canvas id="the-canvas"></canvas>\n' +
        '    </main>\n' +
        '</div>')

    // Loaded via <script> tag, create shortcut to access PDF.js exports.
    let pdfjsLib = window['pdfjs-dist/build/pdf'];

    let scrollPosX;
    let scrollPosY;

    $('.preview-document.activePreview').click(function(e) {// Asynchronous download of PDF
        let loading = document.getElementById("loading");
        if (!loading) {
            $('body').append('<div id="loading"\n' +
                '     style="position: fixed; width: 100%;height: 100%; left: 0; top: 0; background: rgba(51, 51, 51, 0.85); display: none">\n' +
                '    <div class="d-flex justify-content-center" style="margin-top: 50vh;">\n' +
                '        <div class="loader"></div>\n' +
                '    </div></div>');
            loading = document.getElementById("loading");
        }

        loading.style.display = 'block';

        scrollPosX = window.scrollX;
        scrollPosY = window.scrollY;
        let loadingTask = pdfjsLib.getDocument($(this).data('docurl'));
        loadingTask.promise.then(function(pdf) {
            console.log('PDF loaded');
            loading.style.display = 'none';
            // Fetch the first page
            let pageNumber = 1;
            pdf.getPage(pageNumber).then(function(page) {
                console.log('Page loaded');

                // Prepare canvas using PDF page dimensions
                let canvas = document.getElementById('the-canvas');
                let context = canvas.getContext('2d');
                /*canvas.height = viewport.height;
                canvas.width = viewport.width;*/
                canvas.width = document.body.clientWidth;
                let scale = (canvas.width / page.view[2]);
                let viewport = page.getViewport({scale: scale});
                canvas.height = page.view[3] * scale;

                // Render PDF page into canvas context
                var renderContext = {
                    canvasContext: context, // Initialized in a higher scope using: ctx = canvas.getContext('2d');
                    viewport: viewport
                };
                console.log( "Width: " + viewport.width + ", Height: " + viewport.height );
                console.log(screen.width)
                console.log(screen.height)


               /* // Render PDF page into canvas context
                let renderContext = {
                    canvasContext: context,
                    viewport: viewport
                };*/
                let renderTask = page.render(renderContext);
                renderTask.promise.then(function () {
                    console.log('Page rendered');
                });
            });
        }, function (reason) {
            // PDF loading error
            console.error(reason);
        });

        e.preventDefault();
        $('html, body').css('overflow', 'hidden');
        $('#viewer').addClass('visible');
    });

    $('.close-preview').click(function() {
        $('html, body').removeAttr('style');
        $('#viewer').removeClass('visible');
        window.scrollTo({
            top: scrollPosY,
            left: scrollPosX,
            behavior: "instant",
        });
    });

    $("#the-canvas").click(function (e) {
        e.stopPropagation(); // or add this
    });

});
