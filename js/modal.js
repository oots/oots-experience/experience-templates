let timeout = 5000;
const urlHref = new URL(window.location.href);
const pageModal = urlHref.pathname;

function attachOpenModalsButtonsEvent() {
    const modalButtons = document.getElementsByClassName('open-modal');
    Array.from(modalButtons).forEach((el) => {
        el.addEventListener('click', function () {
            openModal(el.dataset.modalid, el.dataset.destination);
        });
    });
}

function openModal(modalId, destination) {
    closeAllModals();
    let loading = document.getElementById("loading");
    let modalDiv = document.getElementById(modalId);
    const destinationAfterSignIn = sessionStorage.getItem('destination-after-sign-in');
    loading.style.display = 'block';
    if (modalId !== 'redirecting' || !destinationAfterSignIn || destinationAfterSignIn.indexOf("evidence-preview-confirm") === -1) {
        modalDiv.style.display = 'block';
    }

    if (modalId === 'redirecting' || modalId === 'sharing') {
        if (pageModal.includes('NAS') && destinationAfterSignIn) {
            setTimeout(function () {
                window.location.href = destinationAfterSignIn;
            }, timeout)
        } else {
            if (destination) {
                setTimeout(function () {
                    window.location.href = destination;
                }, timeout)
            }
            if (modalId === 'redirecting' && sessionStorage.getItem('destination')) {
                setTimeout(function () {
                    window.location.href = sessionStorage.getItem('destination');
                }, timeout)
            }
        }
    }

}

function closeAllModals() {
    let modals = document.getElementsByClassName('modal');
    Array.from(modals).forEach((el) => {
        el.style.display = 'none';
    });
}

attachOpenModalsButtonsEvent();
